#include <cnats/nats.h>

int main() {
  natsOptions* opts;
  natsOptions_Create(&opts);
  natsOptions_Destroy(opts);
  return 0;
}
