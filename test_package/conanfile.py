from conans import ConanFile, CMake
import os

channel = os.getenv("CONAN_CHANNEL", "1.4.0")
username = os.getenv("CONAN_USERNAME", "genvidtech")

class CNatsTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    requires = "cnats/1.5.0@%s/%s" % (username, channel)
    generators = "cmake"

    def build(self):
        cmake = CMake(self.settings)
        self.run('cmake "%s" %s' % (self.conanfile_directory, cmake.command_line))
        self.run("cmake --build . %s" % cmake.build_config)

    def imports(self):
        self.copy("*.dll", "bin", "bin")
        self.copy("*.dylib", "bin", "lib")

    def test(self):
        os.chdir("bin")
        self.run(".%scnats-test" % os.sep)

