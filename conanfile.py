from conans import ConanFile, CMake, tools
import os


class CnatsConan(ConanFile):
    name = "cnats"
    version = "1.5.0"
    license = "MIT"
    url = "https://bitbucket.org/genvidtech/conan-cnats"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "tls": [True, False], "genvid_patch":[True, False]}
    default_options = "shared=False", "tls=False", "genvid_patch=True"
    generators = "cmake"
    description = "C implementation of NATS client."
    exports = "*"
    dllmain_name_define="NATS_DLLMAIN_NAME=nats_DllMain"

    def source(self):
       self.run("git clone https://github.com/nats-io/cnats.git")
       self.run("cd cnats && git checkout v%s" % self.version)
       tools.replace_in_file("cnats/CMakeLists.txt", "project(cnats)", '''project(cnats)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')
       if self.options.genvid_patch:
           tools.patch(base_path="cnats", patch_file="genvid.patch", strip=1)

    def must_defined_dllmain(self):
        return not self.options.shared and \
            self.options.genvid_patch and \
            self.settings.os == "Windows"

    def build(self):
        cmake = CMake(self.settings)
        options = []
        if self.options.shared:
            options.append("-DBUILD_SHARED_LIBS=ON")
        if self.must_defined_dllmain():
            options.append("-DCMAKE_C_FLAGS=/D" + self.dllmain_name_define)
        if not self.options.tls:
            options.append("-DNATS_BUILD_WITH_TLS=OFF")
        opts = " ".join(options)
        self.run('cmake cnats %s %s' % (cmake.command_line, opts))
        self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.h", dst="include/cnats", src="cnats/src")
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["nats" if self.options.shared else "nats_static" ]
        if self.must_defined_dllmain():
            self.cpp_info.defines = [ self.dllmain_name_define ]
